#!/usr/bin/env python
#-*- coding:utf-8 -*-


"""
Toma todos los logs de logs_path y los distribuye en un archivo por usuario, en la
carpeta dest_path/username.logfile.txt,

Si se produce un error en alguna línea, la guarda en error_lines.txt.

Las líneas vacías se tiran.

Sript para monitorear dest_path:
    while [ 1 ]; do clear; ls; du -sh; wc error_lines.txt; sleep 5; done

"""


from os import listdir


logs_path = "/home/tmoreyra/projects/tesis/raw_data/logs/"
dest_path = "/home/tmoreyra/projects/tesis/ordered_data/logs_by_user/"


user_files = {}  # Dict: username::str -> userfile::file
error_file = open(dest_path + 'error_lines.txt', 'w')  # File to log files with errors


for logfile in listdir(logs_path):
    for line in open(logs_path + logfile).readlines():
        try:
            line_split = line.replace('\r', ' ').replace('\n', ' ').split('|')
            if line_split[2] == 'key sys down':
                user = line_split[5].lower()
            else:
                user = line_split[4].lower()

            try:
                user_files[user].write(line)
            except KeyError:
                user_files[user] = open(dest_path + user +  '.logfile.txt', 'w')
                user_files[user].write(line)
        except:
            if line != '\r\n':
                error_file.write(line)
