#!/usr/bin/env python
#-*- coding:utf-8 -*-

TEST = "iris_bagging_test.arff"
PRED = "iris_predictions_%d.txt"

MODELS = 150

def result(d):
    win, count = max(d.items(), key=lambda x: x[1])
    return win, round(float(count)/MODELS, 2)
    

def main():
    realclass = []

    f = open(TEST)
    line = f.readline()
    while line:
        realclass.append(line[:-1].split(",")[-1])  # Agrego la clase
        line = f.readline()

    realclass = realclass[10:]  # Saco el header
    
    CS = ['Iris-set', 'Iris-ver', 'Iris-vir']
    n = len(realclass)
    m = len(CS)
    
    predictions = {}
    for i, c in enumerate(realclass, 1):
        predictions[i] = (c[:8], dict(zip(CS, [0]*m)))
    
    # predictions[0] ('Iris-setosa', {'Iris-virginica': 0,
    #                                 'Iris-setosa': 0,
    #                                 'Iris-versicolor': 0})

    for i in range(1, MODELS + 1):
        f = open(PRED % i)
        line = f.readline().split()
        while line:
            inst = int(line[0])
            vote = line[2].split(":")[1]
            predictions[inst][1][vote] += 1
            line = f.readline().split()
            
    cci = 0
    for k, v in predictions.iteritems():
        if result(v[1])[0] == v[0]:
            cci += 1
        print k, result(v[1]), v
    
    print cci, float(cci)/len(predictions)

if __name__ == '__main__':
    main()


