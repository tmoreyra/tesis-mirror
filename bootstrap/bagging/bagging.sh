filename=iris
models=150 # Cuántos modelos genero para que "voten"

training=${filename}_bagging_sample.arff
test=${filename}_bagging_test.arff

# Elijo por ahora solo el j48, despues iteraré entre varios
classifier=trees.J48

weka=../../weka.jar

for i in `seq 1 $models`
do
    # Creo un sample del 100% con reposición
    echo "Creando subsample"
    java -Xmx2048m -cp $weka weka.filters.unsupervised.instance.Resample \
        -S $i \
        -Z 100 \
        -i $training \
        -o ${filename}_bagging_subsample_$i.arff

    echo "Creando modelo"
    # Creo un modelo para el nuevo sample y lo guardo
    java -Xmx2048m -cp $weka weka.classifiers.$classifier \
        -t ${filename}_bagging_subsample_$i.arff \
        -T $test \
        -p 0 \
        | grep : \
        > ${filename}_predictions_$i.txt
done
