filename=iris
percent=20 # Porcentaje que tomo de training, el resto es para test
seed=1  # Para poder hacer experimentos distintos

inputarff=$filename.arff
samplearff=${filename}_bagging_sample.arff
testarff=${filename}_bagging_test.arff

weka=../../weka.jar

java -Xmx2048m -cp $weka weka.filters.unsupervised.instance.Resample \
    -S $seed \
    -Z $percent \
    -no-replacement \
    -i $inputarff \
    -o $samplearff \

java -Xmx2048m -cp $weka weka.filters.unsupervised.instance.Resample \
    -S $seed \
    -Z $percent \
    -no-replacement \
    -i $inputarff \
    -o $testarff \
    -V
