#!/usr/bin/env python
#-*- coding:utf-8 -*-

T = 0.95

"""
Toma por stdin predicciones del weka, y devuelve un string de valores
separados por coma, corespondientes a las instancias cuya confianza de
clasificación es menor T.
"""

ret = []
while True:
    try:
        pred = raw_input()
    except EOFError:
        break
    
    pred = pred.replace('+', '')
    pred = pred.split()
        
    if float(pred[3]) < T:
        ret.append(pred[0])
        
print ",".join(ret)


