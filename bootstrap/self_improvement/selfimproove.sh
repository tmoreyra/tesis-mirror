filename=iris
percent=30 # Porcentaje que tomo de training, el resto es para test
seed=2  # Para poder hacer experimentos distintos

inputarff=$filename.arff
trainingset=${filename}_training.arff
testset=${filename}_test.arff
classifier=trees.J48

weka=../../weka.jar

ITER=3

echo "*** Creando Training-Set ***"
java -Xmx2048m -cp $weka weka.filters.unsupervised.instance.Resample \
    -S $seed \
    -Z $percent \
    -no-replacement \
    -i $inputarff \
    -o $trainingset \
    
cat -n $trainingset

echo "*** Creando Test-Set ***"
java -Xmx2048m -cp $weka weka.filters.unsupervised.instance.Resample \
    -S $seed \
    -Z $percent \
    -no-replacement -V \
    -i $inputarff \
    -o $testset \

cat -n $testset

for i in `seq $ITER`
do
    echo "*** Creando modelo ***"
    java -Xmx2048m -cp $weka weka.classifiers.$classifier \
        -t $trainingset \
        -T $testset \
        -p 0 \
        | grep : \
        > predictions.tmp

    cat -n predictions.tmp

    echo "*** Mejores clasificadas ***"
    range=`./worstpredictions.py < predictions.tmp`
    
    echo "Instancias a sacar: "$range
    java -Xmx2048m -cp $weka weka.filters.unsupervised.instance.RemoveRange \
        -R $range \
        -i $testset \
        -o bests.arff

    cat -n bests.arff

    echo "*** Peores clasificadas ***"
    java -Xmx2048m -cp $weka weka.filters.unsupervised.instance.RemoveRange \
        -R `./worstpredictions.py < predictions.tmp` \
        -V \
        -i $testset \
        -o worsts.arff

    cat -n worsts.arff

    echo "*** Defino el nuevo training-set *** "
    tail -n +10 bests.arff > bests.tmp
    cat $trainingset bests.tmp > $trainingset.tmp
    rm bests.tmp
    mv $trainingset.tmp $trainingset
    cat -n $trainingset

    echo "*** Defino el nuevo test-set ***"
    mv worsts.arff $testset
    cat -n $testset

    echo "*****************************************************"
done


