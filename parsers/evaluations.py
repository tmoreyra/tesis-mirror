#!/usr/bin/env python
#-*- coding:utf-8 -*-

import csv
import sys
import time

from report import Report, Call
from username import username

evaluations_file = "evaluaciones/evaluacionesmayo.csv"

classes = [
 'Pre_Pre',
 'Pre_Pre%',
 'Pre_Ind',
 'Pre_Ind%',
 'Cor_Emp',
 'Cor_Emp%',
 'Cor_Voc',
 'Cor_Voc%',
 'Neg_Ind_Mot',
 'Neg_Ind_Mot%',
 'Neg_Ind_Efe',
 'Neg_Ind_Efe%',
 'Neg_Per_Ord',
 'Neg_Per_Ord%',
 'Neg_Per_TCP',
 'Neg_Per_TCP%',
 'Neg_Per_TCT',
 'Neg_Per_TCT%',
 'Neg_Per_Her',
 'Neg_Per_Her%',
 'Neg_Ana_Sis',
 'Neg_Ana_Sis%',
 'Con_Dat',
 'Con_Dat%',
 'Con_Inf',
 'Con_Inf%',
 'Con_IDT',
 'Con_IDT%',
 'Cie_Tie',
 'Cie_Tie%',
 'Cie_Ges',
 'Cie_Ges%',
 'Cie_Bri',
 'Cie_Bri%',
 'Error_Inaceptable_SN',
 'Error_Inaceptable_M',
 'Total Escucha%']


def classname(s):
    return s.replace('_', '').replace('%', '00').replace(' ', '')


evaluation_items = [classname(s) for s in classes]


class UnusefulEvaluation(Exception):
    pass


class Evaluation(object):
    """
    """
    def __init__(self, csv_dict, call_ends):
        self.csv_dict = csv_dict
        self.call_ends = call_ends

        self.set_start()
        self.set_user()
        self.set_end()
        self.items = ((classname(item), self.csv_dict[item]) for item in classes)

    def set_start(self):
        start = self.csv_dict['Fecha Audio Evaluado'] + \
                self.csv_dict['Hora Inicio Audio']
        try:
            start = time.strptime(start, '%d/%m/%y%H:%M:%S 0.0')
            self.start = int(time.mktime(start))
        except ValueError:
            try:
                start = time.strptime(start, '%d-%m-%y%H:%M:%S 0.0')
                self.start = int(time.mktime(start))
            except ValueError as err:
                raise UnusefulEvaluation('Bad date: %s' % err.message)

    def set_user(self):
        realname = self.csv_dict['Operador'].split(',')[0]
        if realname not in username or not username[realname]:
            raise UnusefulEvaluation('No username for %s' % realname)
        else:
            self.user = username[realname]

    def set_end(self):
        try:
            self.end = self.call_ends[self.user][self.start]
        except KeyError as err:
            raise UnusefulEvaluation('No call end: %s' % err.message)


def get_call_ends(path):
    ret = {}
    for call in Report(path, Call):
        if call.user in username and username[call.user]:
            user = username[call.user]
            try:
                ret[user][call.start] = call.end
            except KeyError:
                ret[user] = {call.start: call.end}

    return ret


def get_evaluations(filename, callreport_path):
    """
    """
    call_ends = get_call_ends(callreport_path)
    evaluations = csv.DictReader(open(evaluations_file), delimiter=';')

    ret = {}
    for evaluation in evaluations:
        try:
            e = Evaluation(evaluation, call_ends)
        except UnusefulEvaluation as err:
            print err.message
            continue

        try:
            ret[e.user].append(e)
        except KeyError:
            ret[e.user] = [e]

    return ret


if __name__ == '__main__':
    ret = get_evaluations('evaluations_file', 'reportes_llamadas')
    import pdb; pdb.set_trace()
