#!/usr/bin/env python
#-*- coding:utf-8 -*-

from intervalfile import IntervalFile, show_error
from datum import LogDatum


class LogFile(IntervalFile):

    def __init__(self, path, interval_len):
        super(LogFile, self).__init__(path, interval_len)
        
        self.atom_attribute_methods = []
        self.comp_attribute_methods = []

        for meth in dir(self):
            if meth.startswith('attr_atom_'):
                self.atom_attribute_methods.append(getattr(self, meth))
            elif meth.startswith('attr_'):
                self.comp_attribute_methods.append(getattr(self, meth))

    def __iter__(self):
        return self

    def fill_datum(self):
        """
        Obtiene un nuevo intervalo de datos crudos, crea un LogDatum vacío,
        corre todos los métods de atributos atómicos para cada uno de los
        datos crudos del nuevo intervalo y después todos los métodos de
        atributos compuestos
        """
        self.datum = LogDatum()
        prev = LogDatum()
        for raw_datum in self.get_interval():
            # print raw_datum
            for meth in self.atom_attribute_methods:
                meth(raw_datum, prev)
            prev = raw_datum

        for meth in self.comp_attribute_methods:
            meth()

    def get_datum(self):
        """
        Devuelve un registro separado por comas con los valores de self.datum
        en el orden en que los da self.attributes
        """
        d = []

        for a in self.attributes():
            d.append(getattr(self.datum, a))

        return ",".join(map(str, d))

    def next(self):
        """ Para usar en el `for` """
        self.fill_datum()
        return self.get_datum()

    def attributes(self):
        """
        Devuelve una lista de nombres de atributos, ordenados según los nombres
        de los métodos.
        """
        attrs = [attr_reverse(s[5:].replace('atom_', ''))
                                for s in dir(self) if s.startswith('attr_')]
        attrs.sort()
        for i in range(len(attrs)):
            attrs[i] = attrs[i].split('_')[-1]

        return attrs


def attr_reverse(s):
    "a_b_c -> c_b_a"

    a = s.split('_')
    a.reverse()
    return '_'.join(a)
