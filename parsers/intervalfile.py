#!/usr/bin/env python
#-*- coding:utf-8 -*-

from concatfiles import ConcatFiles
from datum import LogRawDatum, BadLine


def show_error(msg):
    print msg


class IntervalFile(ConcatFiles):
    """
    Archivo de intervalos. Exporta el método get_interval que me devuelve el
    siguiente intervalo como una lista de LogRawDatum
    """
    def __init__(self, path, interval_len):
        super(IntervalFile, self).__init__(path)
        self.interval_len = interval_len
        self.saved_datum = None

    def __get_raw_datum(self):
        """
        Devuelve un objeto LogRawDatum con el siguiente dato crudo a utilizar.
        """
        if self.saved_datum is not None:
            ret = self.saved_datum
            self.saved_datum = None
            return ret

        try:
            return LogRawDatum(self.readline())
        except BadLine as e:
            show_error(e.message)
            return self.__get_raw_datum()

    def get_interval(self):
        """
        Devuelve una lista de LogRawDatum que caigan en el mismo intervalo
        del largo indicado
        """
        rd = self.__get_raw_datum()
        a = rd.time - rd.time % self.interval_len
        b = a + self.interval_len

        ret = []
        while a <= rd.time and rd.time < b:  # rd.time in [a, b)
            ret.append(rd)

            try:
                rd = self.__get_raw_datum()
            except StopIteration:
                return ret

        self.saved_datum = rd
        return ret


if __name__ == '__main__':
    count = 0
    f = IntervalFile('logs', 120)
    while True:
        try:
            count += len(f.get_interval())
        except StopIteration:
            break

    print count
