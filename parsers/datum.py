#!/usr/bin/env python
#-*- coding:utf-8 -*-

from time import mktime, strptime


class BadLine(Exception): pass


class LogDatum(object):
    """
    Esto después se representa como un dato del .arff
    """
    def __getattribute__(self, attr):
        """
        Cualquier atributo desconocido que pida, es 0.
        """
        try:
            return super(LogDatum, self).__getattribute__(attr)
        except AttributeError:
            return 0

    def __str__(self):
        """
        Muestro todos los atributos y su valor
        """
        attrs = [(a, getattr(self, a)) for a in dir(self)
                                       if not a.startswith('__')]
        return '\n'.join(['%s: %s' % (a, str(v)) for a, v in attrs if v])


class LogRawDatum(object):
    """
    Objeto para representar un dato crudo del log
    """
    def __init__(self, line):
        """
        Se inicializa con una línea del archivo de logs.
        """
        try:
            self.show = line[:-2]  # for debug
            line = line[:-2].split('|')  # cut '\r\n' and split

            self.time = int(mktime(strptime(line[0] + line[1], '%Y%m%d%H:%M:%S')))
            self.app_path = line[2].lower()
            self.window = line[3]
            self.user = line[4].lower()
            self.window_name = line[5].lower()

            self.is_click = False
            self.is_key = False
            self.is_syskey = False

            if line[6] in ['left', 'right']:
                self.is_click = True
                self.click_type = line[6]
                self.click_pos = tuple(map(int, line[7].strip('()').split(',')))
            elif line[2] == 'key sys down':
                self.app_path = line[3]
                self.window = line[4]
                self.user = line[5].lower()
                self.window_name = line[6]
                self.is_syskey = True
                self.key_type = line[7]
            else:
                self.is_key = True
                self.key_type = line[6]

        except Exception as e:
            raise BadLine('Error [%s] in:\n%s' % (e.message, self.show))

    def __str__(self):
        attrs = [(a, getattr(self, a)) for a in dir(self)
                                       if not a.startswith('__')]
        #return '\n'.join(['%s: %s' % (a, str(v)) for a, v in attrs])
        return self.show

    def __eq__(self, other):
        if self.is_click and other.is_click and \
            self.click_type == 'left' and other.click_type == 'left' and \
            self.click_pos == other.click_pos and self.time == other.time:
            return True
        else:
            return False
