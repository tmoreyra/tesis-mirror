#!/usr/bin/env python
#-*- coding:utf-8 -*-

import time
from concatfiles import ConcatFiles


def str_to_sec(ss):
    try:
        h, m, s = map(int, ss.split(':'))
    except:
        raise Exception('Cannot convert %s' % ss)

    return s + m*60 + h*60*60


class Report(ConcatFiles):
    """
    """
    def __init__(self, path, return_type):
        self.ret_type = return_type
        super(Report, self).__init__(path)

        self.SKIP_WORDS = ['LLAMADAS', 'Fecha', 'Total', 'TOTALES',
                           'ACUMULADORES', '"";"";"";""']

    def __iter__(self):
        return self

    def next(self):
        while True:
            line = self.readline()
            if 'LoginID' in line:
                # Línea con user -> Lo guardo y leo otra
                line = line[:-1].strip('"').split('-')
                self.user = line[0].strip(' ')

            elif any(w in line for w in self.SKIP_WORDS) or len(line) < 32:
                # Línea que no sirve -> Borro el user y leo otra
                self.user = None

            elif self.user is None:
                # Línea que sirve, pero no tengo user
                raise Exception(line)

            else:
                return self.ret_type(self.user, line)


class Call(object):
    """
    """
    def __init__(self, user, report_line):
        self.user = user

        report_line = report_line.split(';')

        time_format = '"%d/%m/%Y %I:%M:%S %p"'
        t = time.strptime(report_line[0].replace('.', ''), time_format)
        self.start = int(time.mktime(t))

        d = str_to_sec(report_line[1].strip('"'))
        self.end = self.start + d

    def __str__(self):
        return '(%s, %d, %d)' % (self.user, self.start, self.end)
