#!/usr/bin/env python
#-*- coding:utf-8 -*-

from math import sqrt
from logfile import LogFile


UPPER_KEYS = ["[[%s]]" % chr(k) for k in range(65, 91)] + ["Ñ", "A", "€"]

SYMBOL_KEYS = ["º", "ª", "¡", "¿", "[[Space]]", "[[Add]]", "[[Substract]]"] + \
    map(chr, range(32,48) + range(58, 65) + range(91, 97) + range(123, 127))

NUM_KEYS = ["1"] + ["[[%d]]" % n for n in range(10)] + \
           ["[[Numpad%d]]" % n for n in range(10)]

FN_KEYS = ["[[F%d]]" % n for n in range(1, 13)]

def dist(p, q):
    p1, p2 = p
    q1, q2 = q
    return sqrt((p1-q1)**2 + (p2-q2)**2)


class Arff(LogFile):
    def header(self):
        return ",".join(self.attributes())

    def attr_alpha(self):
        self.datum.alpha = self.attr_lowercase_alpha() + self.attr_uppercase_alpha()
        return self.datum.alpha

    def attr_lowercase_alpha(self):
        self.datum.lowercase = self.datum.lowercommon + self.datum.loweracute
        return self.datum.lowercase

    def attr_atom_lowercommon_lowercase_alpha(self, rd, prev):
        if rd.is_key and rd.key_type in ('a', 'ñ'):
            self.datum.lowercommon += 1

    def attr_atom_loweracute_lowercase_alpha(self, rd, prev):
        if rd.is_key and rd.key_type in ('á', 'é', 'í', 'ó', 'ú'):
            self.datum.loweracute += 1

    def attr_uppercase_alpha(self):
        self.datum.uppercase = self.datum.uppercommon + self.datum.upperacute
        return self.datum.uppercase

    def attr_atom_uppercommon_uppercase_alpha(self, rd, prev):
        if rd.is_key and rd.key_type in UPPER_KEYS:
            self.datum.uppercommon += 1

    def attr_atom_upperacute_uppercase_alpha(self, rd, prev):
        if rd.is_key and rd.key_type in ('Á', 'É', 'Í', 'Ó', 'Ú'):
            self.datum.upperacute += 1

    def attr_mostused(self):
        key = lambda app: getattr(self.datum, app)
        self.mostused = max('stealth', 'iexplore', 'explorer', 'office',
                            'tbnote', 'ippad', 'noprocname', key=key)
        return self.mostused

    def attr_atom_stealth_mostused(self, rd, prev):
        if 'stealth' in rd.window_name:
            self.datum.stealth += 1

    def attr_atom_iexplore_mostused(self, rd, prev):
        if 'iexplore' in rd.app_path.split("\\")[-1] and not 'stealth' in rd.window_name:
            self.datum.iexplore += 1

    def attr_atom_explorer_mostused(self, rd, prev):
        if 'explorer' in rd.app_path.split("\\")[-1]:
            self.datum.explorer += 1

    def attr_atom_notepad_mostused(self, rd, prev):
        if 'notepad' in rd.app_path.split("\\")[-1]:
            self.datum.notepad += 1

    def attr_atom_office_mostused(self, rd, prev):
        if 'office' in rd.app_path.split("\\")[-1]:
            self.datum.office += 1

    def attr_atom_tbnote_mostused(self, rd, prev):
        if 'tbnote' in rd.app_path.split("\\")[-1]:
            self.datum.tbnote += 1

    def attr_atom_ippad_mostused(self, rd, prev):
        if 'ippad' in rd.app_path.split("\\")[-1]:
            self.datum.ippad += 1

    def attr_atom_noprocname_mostused(self, rd, prev):
        if 'noprocname' in rd.app_path.split("\\")[-1]:
            self.datum.noprocname += 1

    def attr_atom_left_clicks(self, rd, prev):
        if rd.is_click and rd.click_type == 'left':
            self.datum.left += 1

    def attr_atom_right_clicks(self, rd, prev):
        if rd.is_click and rd.click_type == 'right':
            self.datum.right += 1

    def attr_atom_doubleclick_clicks(self, rd, prev):
        # FIXME Capaz no haga falta el `prev` en todos
        if rd == prev and not getattr(self, 'doubleclick_flag', False):
            # notar que `==` está redefinido
            self.datum.doubleclick += 1
            self.datum.left -= 2
            self.doubleclick_flag = True
        else:
            # Para no contar un "triple" click como 2 dobles.
            self.doubleclick_flag = False

    def attr_atom_movement_clicks(self, rd, prev):
        if rd.is_click:
            prev_click_pos = getattr(self, 'prev_click_pos', rd.click_pos)
            self.datum.movement += round(dist(rd.click_pos, prev_click_pos), 2)
            self.prev_click_pos = prev_click_pos

    def attr_clicks(self):
        self.datum.clicks = self.datum.left + self.datum.right + \
                            self.datum.doubleclick
        return self.datum.clicks

    def attr_atom_arrowup_arrow(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type == '[[Up]]':
            self.datum.arrowup += 1

    def attr_atom_arrowdown_arrow(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type == '[[Down]]':
            self.datum.arrowdown += 1

    def attr_atom_arrowright_arrow(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type == '[[Right]]':
            self.datum.arrowright += 1

    def attr_atom_arrowleft_arrow(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type == '[[Left]]':
            self.datum.arrowleft += 1

    def attr_arrow(self):
        self.datum.arrow = self.datum.arrowup + self.datum.arrowdown + \
                           self.datum.arrowright + self.datum.arrowleft
        return self.datum.arrow

    def attr_atom_copy_copypaste(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type in ('\x03', '[[C]]'):
            self.datum.copy += 1
            self.ctrl = max(0, self.datum.ctrl-1)

    def attr_atom_paste_copypaste(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type in ('\x16', '[[V]]'):
            self.datum.paste += 1
            self.ctrl = max(0, self.datum.ctrl-1)

    def attr_atom_cut_copypaste(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type in ('\x18', '[[X]]'):
            self.datum.cut += 1
            self.ctrl = max(0, self.datum.ctrl-1)

    def attr_copypaste(self):
        self.datum.copypaste = self.datum.copy + self.datum.paste \
                               + self.datum.cut
        return self.datum.copypaste

    def attr_atom_home_homeend_system(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type == '[[Home]]':
            self.datum.home += 1

    def attr_atom_end_homeend_system(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type == '[[End]]':
            self.datum.end += 1

    def attr_atom_insert_system(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type == '[[Insert]]':
            self.datum.insert += 1

    def attr_atom_ctrl_system(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type in ('[[Lcontrol]]',
                                                           '[[Rcontrol]]'):
            self.datum.ctrl += 1

    def attr_atom_alt_system(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type in ('[[Lmenu]]',
                                                           '[[Rmenu]]'):
            self.datum.alt += 1

    def attr_atom_fn_system(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type in FN_KEYS:
            self.datum.fn += 1

    def attr_atom_esc_system(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type == '[[Escape]]':
            self.datum.esc += 1

    def attr_atom_othersys_system(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type in ('[[Lwin]]',
                '[[Rwin]]', '[[Numlock]]', '[[Clear]]'):
            self.datum.othersys += 1

    def attr_atom_space_symbol(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type in (' ', '[[Space]]'):
            self.datum.space += 1

    def attr_atom_quotes_symbol(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type == '"':
            self.datum.quotes += 1
            self.datum.null = max(0, self.datum.null-1)

    def attr_atom_parenthesis_symbol(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type in ('(', ')'):
            self.datum.parenthesis += 1
            self.datum.null = max(0, self.datum.null-1)

    def attr_atom_add_symbol(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type in ('+', '[[Add]]'):
            self.datum.add += 1

    def attr_atom_mult_symbol(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type == '*':
            self.datum.mult += 1

    def attr_atom_subs_symbol(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type in ('-', '[[Substract]]'):
            self.datum.subs += 1

    def attr_atom_comma_symbol(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type == ',':
            self.datum.comma += 1

    def attr_atom_dot_symbol(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type == '.':
            self.datum.dot += 1

    def attr_atom_colon_symbol(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type == ':':
            self.datum.colon += 1
            self.datum.null = max(0, self.datum.null-1)

    def attr_atom_semicolon_symbol(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type == ';':
            self.datum.semicolon += 1
            self.datum.null = max(0, self.datum.null-1)

    def attr_atom_questexcl_symbol(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type in ('¡', '¿', '?', '!'):
            self.datum.questexcl += 1

    def attr_atom_underscore_symbol(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type == '_':
            self.datum.underscore += 1
            self.datum.null = max(0, self.datum.null-1)

    def attr_atom_percent_symbol(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type == '%':
            self.datum.percent += 1
            self.datum.null = max(0, self.datum.null-1)

    def attr_atom_symbol(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type in SYMBOL_KEYS:
            self.datum.symbol += 1

    def attr_othersymbol_symbol(self):
        self.datum.othersymbol = self.datum.symbol - self.datum.percent - \
                self.datum.underscore - self.datum.questexcl - \
                self.datum.semicolon - self.datum.colon - self.datum.dot - \
                self.datum.comma - self.datum.subs - self.datum.add - \
                self.datum.mult - self.datum.parenthesis - self.datum.space - \
                self.datum.quotes
        assert self.datum.othersymbol >= 0
        return self.datum.othersymbol

    def attr_atom_number(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type in NUM_KEYS:
            self.datum.number += 1

    def attr_atom_returnkey_otherkeys(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type == '[[Return]]':
            self.datum.returnkey += 1

    def attr_atom_backspace_otherkeys(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type == '[[Back]]':
            self.datum.backspace += 1

    def attr_atom_delete_otherkeys(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type == '[[Delete]]':
            self.datum.delete += 1

    def attr_atom_tab_otherkeys(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type == '[[Tab]]':
            self.datum.tab += 1

    def attr_atom_null_otherkeys(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type == '\x00':
            self.datum.null += 1

    def attr_atom_bell_otherkeys(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type == '\x07':
            self.datum.bell += 1

    def attr_atom_undo_otherkeys(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type == '\x1a':
            self.datum.undo += 1
            self.datum.ctrl = max(0, self.datum.ctrl-1)
 
    def attr_atom_alttab_otherkeys(self, rd, prev):
        if (rd.is_key or rd.is_syskey) and rd.key_type == '\t':
            self.datum.alttab += 1 

    def attr_homeend_system(self):
        self.datum.homeend = self.datum.home + self.datum.end
        return self.datum.homeend

    def attr_system(self):
        self.datum.system = self.attr_homeend_system() + self.datum.insert + \
                self.datum.ctrl + self.datum.alt + self.datum.fn + \
                self.datum.othersys

    def attr_atom_windowchanges(self, rd, prev):
        if rd.window != prev.window:
            self.datum.windowchanges += 1

    def attr_atom_user(self, rd, prev):
        if not self.datum.user:
            self.datum.user = rd.user

    def attr_atom_halfhourinterval(self, rd, prev):
        if not self.datum.halfhourinterval:
            self.datum.halfhourinterval = rd.time - (rd.time % 1800)

    def attr_atom_handsfree(self, rd, prev):
        if not self.datum.handsfree:
            self.datum.handsfree = rd.time - getattr(self, 'last_event_time', rd.time-1)
            if self.datum.handsfree < 0:
                # Cambié de archivo, uso un default
                # FIXME Chequear que no puede ser un valor muy grande
                self.datum.handsfree = 1  # que sea != 0

        self.last_event_time = rd.time


if __name__ == '**__main__':
    f = Arff('logs', 15)
    while True:
        f.fill_datum()
        print f.datum
        print '*'*50

if __name__ == '__main__':
    arff = Arff('logs', 30)
    
    print arff.header()

    for d in arff:
        print d
