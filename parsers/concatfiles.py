#!/usr/bin/env python
#-*- coding:utf-8 -*-

from os import listdir


class ConcatFiles(object):
    """
    Provee el metodo readline() que permite saltar de la ultima linea de un
    archivo a la primera del siguiente como si fueran consecutivas.
    Se inicializa con el path donde están los archivos.
    """
    def __init__(self, path):
        """
        Se inicializa con el path donde están los archivos a leer.
        """
        self.files = map(lambda f: '%s/%s' % (path, f), listdir(path))

        try:
            self.file = open(self.files.pop())
        except IndexError:
            raise Exception('No files in %s' % path)

    def readline(self):
        line = ''
        while not line:
            line = self.file.readline()
            if not line and self.files:
                self.file = open(self.files.pop())
            elif not line and not self.files:
                raise StopIteration  # para el `for`

        return line

if __name__ == '__main__':
    f = ConcatFiles('logs')
    while True:
        print f.readline()[:-2]
